import { Elysia } from 'elysia'
import {swagger} from '@elysiajs/swagger'
import cors from '@elysiajs/cors'

export const app = new Elysia()
    .get('/', () => 'Hello Elysia')
    .get('/ping', () => 'pong pong')
    .use(swagger())
    .use(cors())
    .listen(3005)

console.log(`🦊 Elysia is running at ${app.server?.hostname}:${app.server?.port}`)

export type ElysiaApp = typeof app