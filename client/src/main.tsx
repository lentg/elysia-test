import React, { createContext } from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import { eden } from "@elysiajs/eden";
import type { ElysiaApp } from "../../server/src/index";

const client = eden<ElysiaApp>("http://localhost:3005");
export const EdenClientContext = createContext(client);
client.ping.get().then(console.log)

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <EdenClientContext.Provider value={client}>
      <App />
    </EdenClientContext.Provider>
  </React.StrictMode>
);
